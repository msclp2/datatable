import * as auth from './auth';
import * as storage from './storage';

export const AuthService = auth;
export const StorageService = storage;
