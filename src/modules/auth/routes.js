import Login from './components/Login';
import RemindPassword from './components/RemindPassword';

export default [
  {
    path: 'login',
    component: Login,
  },
  {
    path: 'remind-password',
    component: RemindPassword,
  }
];
