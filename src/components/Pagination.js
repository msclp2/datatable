import React, { PropTypes } from 'react';
import classnames from 'classnames';
import Dropdown from './Dropdown';

const visible = 2;

const minShown = (pageNum) => (pageNum - visible > 1) ? pageNum - visible : 1;
const maxShown = (pageNum, pagesCount) => (pageNum + visible < pagesCount) ? pageNum + visible : pagesCount;

const createPagesArray = (pageNum, pagesCount) => {
  const pagesArray = [];
  for (let i = minShown(pageNum); i <= maxShown(pageNum, pagesCount); i++) {
    pagesArray.push(i);
  }
  return pagesArray;
};

const Pagination = ({ config, onChange }) => {
  const { page, limit, total } = config;
  const pagesCount = Math.ceil(total/limit);
  const handleChangePage = (page) => onChange({ page });
  const handleChangeLimit = (newLimit) => onChange({ page: 1, limit: newLimit });
  return (
    <div className="Pagination">
      <div className="Pagination-nav">
        <nav>
          <a onClick={(e) => handleChangePage(1)}>First</a>
          <a onClick={(e) => handleChangePage(page - 1)}>Prev</a>
          {createPagesArray(page, pagesCount).map((p) => {
            const cls = classnames('', {
              'is-current': page === p,
            });
            return (
              <a
                key={p}
                className={cls}
                onClick={(e) => handleChangePage(p)}>
                {p}
              </a>
            );
          })}
          <a onClick={(e) => handleChangePage(page + 1)}>Next</a>
          <a onClick={(e) => handleChangePage(pagesCount)}>Last</a>
        </nav>
      </div>
      <div className="Pagination-limit">
        <Dropdown
          options={[ 5, 10, 15, 20 ]}
          selected={limit}
          onChange={handleChangeLimit} />
      </div>
    </div>
  );
};

Pagination.propTypes = {
  config: PropTypes.shape({
      page: PropTypes.number.isRequired,
      limit: PropTypes.number.isRequired,
      total: PropTypes.number.isRequired,
    }).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Pagination;
