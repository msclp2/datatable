import * as validate from '../';

describe('Validation', () => {

  it('validate "required" correctly', () => {
    expect(validate.required('')).toMatch(/.+/);
    expect(validate.required('0')).toBeUndefined();
  });

  it('validate "email" correctly', () => {
    expect(validate.email('@')).toMatch(/.+/);
    expect(validate.email('name@acme')).toMatch(/.+/);
    expect(validate.email('name@.com')).toMatch(/.+/);
    expect(validate.email('name@acme.')).toMatch(/.+/);
    expect(validate.email('name@.')).toMatch(/.+/);
    expect(validate.email('')).toBeUndefined();
    expect(validate.email('asd@asd.ad')).toBeUndefined();
    expect(validate.email('ASD@ASD.AD')).toBeUndefined();
  });

  it('validate "number" correctly', () => {
    expect(validate.number('1.')).toMatch(/.+/);
    expect(validate.number('.')).toMatch(/.+/);
    expect(validate.number('123.')).toMatch(/.+/);
    expect(validate.number('123.123.123')).toMatch(/.+/);
    expect(validate.number('0')).toBeUndefined();
    expect(validate.number(0)).toBeUndefined();
    expect(validate.number(1)).toBeUndefined();
    expect(validate.number(.1)).toBeUndefined();
    expect(validate.number(1.1)).toBeUndefined();
  });
});
