export const set = (name, value) => {
  try {
    const serializedValue = JSON.stringify(value)
    localStorage.setItem(name, serializedValue);
  } catch (error) {
    throw new Error(error);
  }
};

export const get = (name) => {
  try {
    const serializedValue = localStorage.getItem(name);
    if (serializedValue === null) {
      return undefined;
    }
    return JSON.parse(serializedValue);
  } catch (error) {
    return undefined;
  }
};

export const remove = (name) => {
  localStorage.removeItem(name);
};

export const clear = () => {
  localStorage.clear();
}
