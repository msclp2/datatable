import React from 'react';
import { connect } from 'react-redux';

let Item = (props) => (
  <div className="Item">
    Show {props.params.id}
  </div>
);

Item = connect()(Item);

export default Item;
