import React, { Component }             from 'react';
import { connect }                      from 'react-redux';
import { getPosts, getPostsPagination } from '../reducers';
import { postsPageRequest }       from '../actions';
import DataTable                        from '../components/DataTable';

class Posts extends Component {

  componentDidMount() {
    this.fetchData();
  }

  fetchData = (pageNum = 1, limit = 10, sort = 'id', order = 'ASC') => {
    this.props.fetchPostsPage(pageNum, limit, sort, order);
  }

  render() {
    const {
      posts,
      pagination,
    } = this.props;
    const tableConfig = {
      columns: [
        { key: 'id', title: 'ID' },
        { key: 'username', title: 'User name', sortable: true },
        { key: 'postTitle', title: 'Post title', sortable: true },
        { key: 'views', title: 'Views', sortable: true },
        { key: 'likes', title: 'Likes', sortable: true },
        { key: 'createdAt', title: 'Created at', sortable: true },
      ],
      pagination,
    };
    return (
      <div className="Posts">
        <DataTable data={posts} config={tableConfig} handler={this.fetchData} />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  posts: getPosts(state.posts),
  pagination: getPostsPagination(state.posts),
});

Posts = connect(
  mapStateToProps,
  { fetchPostsPage: postsPageRequest }
)(Posts);

export default Posts;
