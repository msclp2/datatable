import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Button = ({ label, className, ...props }) => {
  const modifier = className && className.toLowerCase();
  const cls = classnames('Button', {
    [`Button--${modifier}`]: modifier,
  });
  return (
    <button className={cls} {...props}>
      {label}
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string,
};

export default Button;
