import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../actions';
import LoginForm from '../../../components/forms/Login';
import { getErrorMessage } from '../reducers';
import  messages  from '../../../utils/messages';

class Login extends Component {

  handleSubmit = (credentials) => {
    this.props.login(credentials);
  }

  render() {
    return (
      <LoginForm onSubmit={this.handleSubmit} message={this.props.error} />
    );
  }
}

const mapStateToProps = (state) => ({
  error: getErrorMessage(state),
});

Login = connect(
  mapStateToProps,
  {
    login: login.request,
  }
)(Login);

export default Login;
