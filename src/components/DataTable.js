import React, { PropTypes } from 'react';
import moment from 'moment';
import Filters from './Filters';
import Pagination from './Pagination';

const ColumnHead = ({ config, onSort } ) => {
  const component = (config.sortable) ? (
    <th onClick={(e) => onSort({ column: config.key, page: 1 })}>{config.title}</th>
  ) : (
    <th>{config.title}</th>
  );
  return component;
}

const Header = ({ columns, sort }) => {
  return (
    <thead>
      <tr>
        {columns.map((column) => <ColumnHead config={column} onSort={sort} key={column.key} />)}
      </tr>
    </thead>
  );
};

const Row = ({ data, properties }) => (
  <tr>
    {properties.map((property) => (
      <td key={property}>{(property === 'created') ? moment.unix(data[property]).format('MM/DD/YYYY') : data[property]}</td>
    ))}
  </tr>
);

const DataTable = ({
  data,
  config: {
    columns,
    pagination,
    filters,
  },
  handler,
}) => {
  const properties = columns.map((column) => column.key);
  const handleSort = handler;
  const handlePagination = handler;
  const handleFilter = handler;

  return (
    <div className="DataTable">
      <div className="DataTable-fitlers">
        <Filters onSearch={handleFilter} initialValues={filters} />
      </div>
      <div className="DataTable-pagination">
        {pagination.total && <Pagination config={pagination} onChange={handlePagination} />}
      </div>
      <table className="DataTable-table">
        {columns && <Header columns={columns} sort={handleSort} />}
        <tbody>
          {data.map((row) => <Row key={row.id} data={row} properties={properties} />)}
        </tbody>
      </table>
      <div className="DataTable-pagination">
      </div>
    </div>
  );
};

DataTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  config: PropTypes.shape({
    columns: PropTypes.arrayOf(PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      sortable: PropTypes.bool,
    })).isRequired
  }).isRequired,
};

export default DataTable;
