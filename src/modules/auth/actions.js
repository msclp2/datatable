import { createActionTypes, action } from '../factory';

export const LOGIN           = createActionTypes('auth/LOGIN');
export const REMIND_PASSWORD = createActionTypes('auth/REMIND_PASSWORD');
export const LOGOUT          = 'auth/LOGOUT';

export const login = {
  request: (credentials) => action(LOGIN.REQUEST, { credentials }),
  success: (response)    => action(LOGIN.SUCCESS, { response }),
  failure: (error)       => action(LOGIN.FAILURE, { error }),
};

export const remindPassword = {
  request: (credentials) => action(REMIND_PASSWORD.REQUEST, { credentials }),
  success: (response)    => action(REMIND_PASSWORD.SUCCESS, { response }),
  failure: (error)       => action(REMIND_PASSWORD.FAILURE, { error }),
};

export const logout = (message) => action(LOGOUT, { message });
