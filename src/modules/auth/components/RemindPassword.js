import React, { Component }   from 'react';
import { connect }            from 'react-redux';
import { remindPassword } from '../actions';
import RemindPasswordForm     from '../../../components/forms/RemindPassword';
import { getErrorMessage }    from '../reducers';
import  messages              from '../../../utils/messages';

class RemindPassword extends Component {

  handleSubmit = (credentials) => {
    this.props.remind(credentials);
  }

  render() {

    return (
      <RemindPasswordForm onSubmit={this.handleSubmit} message={this.props.error} />
    );
  }
}

const mapStateToProps = (state) => ({
  error: getErrorMessage(state),
});

RemindPassword = connect(
  mapStateToProps,
  {
    remind: remindPassword.request,
  }
)(RemindPassword);

export default RemindPassword;
