import 'isomorphic-fetch';
import { StorageService } from '.'
import { checkStatus, parseJSON, handleError, queryDecode } from './helpers';

const API_ROOT = 'https://api.anpost.ops-dev.pl/';


const callApi = (method) => (endpoint, data) => {
  const query = (method === 'get') && queryDecode(data);
  const body = (method !== 'get') && JSON.stringify(data);
  const fullUrl = (endpoint.indexOf(API_ROOT) < 0) ? `${API_ROOT}${endpoint}${query}` : endpoint;
  const { access_token } = StorageService.get('token');
  return fetch(fullUrl, {
    method,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${access_token}`,
    },
    body,
  })
    .then(handleError)
    .then(checkStatus)
    .then(parseJSON);
};

export const get = callApi('get');
export const post = callApi('post');
