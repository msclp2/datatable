import React from 'react';

const Edit = (props) => (
  <div className="Edit">
    Edit User: {props.params.id}
  </div>
);

export default Edit;
