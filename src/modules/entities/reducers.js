import { combineReducers } from 'redux';
import { PAGE }   from './actions';

export default combineReducers({
  staff: createReducerForEntity('staff'),
});

export const getEntityList = (state, entity) => state[entity].ids && state[entity].ids.map((id) => state[entity].list[id]) || [];
export const getEntityPagination = (state, entity) => state[entity].pagination && state[entity].pagination || {};
export const getEntityFilters = (state, entity) => state[entity].filters && state[entity].filters || {};

function createReducerForEntity(entity) {
  const initialState = {
    ids: [],
    list: {},
    pagination: {
      column: 'id',
      count: true,
      limit: 10,
      page: 1,
      sort: 'ASC',
      total: null,
    },
    filters: [],
  };

  const ids = (state = initialState.ids, action) => {
    switch (action.type) {
      case PAGE.SUCCESS:
        console.log(action);
        return action.results.map((item) => item.id);
      default:
        return state;
    }
  };
  const list = (state = initialState.list, action) => {
    switch (action.type) {
      case PAGE.SUCCESS:
        const nextState = { ...state };
        action.results.forEach((item) => {
          nextState[item.id] = item;
        });
        return nextState;
      default:
        return state;
    }
  };
  const pagination = (state = initialState.pagination, action) => {
    switch (action.type) {
      case PAGE.SUCCESS:
        return { ...state, ...action.pagination };
      default:
        return state;
    }
  };
  const filters = (state = initialState.filters, action) => {
    switch (action.type) {
      case PAGE.SUCCESS:
        return { ...state, ...action.filters };
      default:
        return state;
    }
  };

  const forEntity = (reducer, initialState = {}) => (state = initialState, action) =>
    action.meta && action.meta.entity === entity ? reducer(state, action) : state;

  return combineReducers({
    ids: forEntity(ids, initialState.ids),
    list: forEntity(list, initialState.list),
    pagination: forEntity(pagination, initialState.pagination),
    filters: forEntity(filters, initialState.filters),
  });

  // this soloution does not work
  // return forEntity(combineReducers({
  //   ids,
  //   list,
  //   pagination,
  //   filters,
  // }));
}
