import React from 'react';
import FiltersForm from './forms/Filters';

const Filters = ({ onSearch }) => {
  const handleSubmit = (data) => {
    onSearch(undefined, data);
  };
  return (
    <div className="Filters">
      <FiltersForm onSubmit={handleSubmit} />
    </div>
  );
};

export default Filters;
