import { connect } from 'react-redux';

const buildRoutes = (entities) =>
  [].concat(...entities.map((entity) =>
    [
      {
        path: `${entity.path}`,
        component: connect()(entity.components.List),
      },
      {
        path: `${entity.path}/new`,
        component: connect()(entity.components.Create),
      },
      {
        path: `${entity.path}/:id`,
        component: connect()(entity.components.Item),
      },
      {
        path: `${entity.path}/:id/edit`,
        component: connect()(entity.components.Edit),
      },
    ]
  ));

export default buildRoutes;
