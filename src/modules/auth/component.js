import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from './actions';

class LoginForm extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    const { login } = this.props;
    const username = 'sadmin';
    const password = 'Test2016!123';
    login({ username, password });
  }

  render() {

    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <input type="text" />
          <input type="password" />
          <button type="submit">Login</button>
        </form>
      </div>
    );
  }
}

LoginForm = connect(
  null,
  {
    login: login.request,
  }
)(LoginForm);

export default LoginForm;
