import App                         from './components/App';
import Dashboard                   from './containers/Dashboard';
import authRoutes                  from './modules/auth/routes';
import { isAuthorized }            from './modules/auth/reducers';
import entitiesRoutes              from './modules/entities/routes';
import { store }                   from '.';
import * as UsersComponents        from './containers/Users';
import { isAuthenticated } from './utils/auth';


export default [
  {
    path: '/auth',
    childRoutes: [
      ...authRoutes,
    ],
  },
  {
    path: '/',
    component: App,
    indexRoute: { component: Dashboard },
    onEnter: isAuthenticated,
    childRoutes: [
      ...entitiesRoutes([
        {
          path: 'users',
          components: UsersComponents,
        },
      ]),
      // ...entitiesRoutes(['users', 'posts']),
    ]
  }
];
