import messages from './messages';

const getMessage = (key) => messages[key] || 'Invalid field';

export const required = (value) => (value) ? undefined : getMessage('required');
export const email = (value) => (value && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i.test(value)) ? getMessage('email') : undefined;
export const number = (value) => (value && !/^(\d*.)?\d+$/.test(value)) ? getMessage('number') : undefined;
