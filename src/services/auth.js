import axios from 'axios';
import * as StorageService from './storage';
import {
  checkStatus,
  parseJSON,
  handleError,
  parseData,
} from './helpers';

export const authorize = (credentials) => {
  const grant_type = credentials.password ? 'password' : 'refresh_token';
  const data = {
    client_id: '1_45ltqp8wo0u8w4ocgkoc8kowkg00gco4oc4osskgwgs4go00ks',
    client_secret: 'wvia4sc44lwocwok80ogw0k4owwg80cc0w4gw8wc4cc4c000c',
    grant_type,
    ...credentials,
  }
  return axios
    .post('http://api.anpost.ops-dev.pl/oauth/v2/token', data)
    .then(checkStatus)
    .then(parseData);
};

export const remindPassword = (credentials) => {
  const data = {
    ...credentials,
  }
  return axios
    .post('http://api.anpost.ops-dev.pl/resetting/send-email', data)
    .then(checkStatus)
    .then(parseData);
}

export const logout = () => {
  StorageService.clear();
};

export const setToken = (token) => {
  StorageService.set('token', { ...token, created: Math.floor(Date.now() / 1000) });
};

export const getToken = () => StorageService.get('token');

export const removeToken = () => {
  StorageService.remove('token');
}
