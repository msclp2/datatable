import React, { PropTypes } from 'react';
import {
  reduxForm,
  // Form,
  Field,
} from 'redux-form';

import { required } from '../../utils/validation';

import InputField from './components/InputField';
import Button from './components/Button';

let LoginForm = ({
  handleSubmit,
  onSubmit,
  message,
  submitting,
}) => (
  <form className="Form Form--login" onSubmit={handleSubmit(onSubmit)}>
    <div className="Form-field">
      <Field
        component={InputField}
        validate={[ required ]}
        label="Username"
        name="username"
        type="text"
      />
    </div>
    <div className="Form-field">
      <Field
        component={InputField}
        validate={[ required ]}
        label="Password"
        name="password"
        type="password"
      />
    </div>
    <div className="Form-messages">
      {message && <span>{message}</span>}
    </div>
    <div className="Form-footer">
      <Button label="Login" type="submit" disabled={submitting} />
    </div>
  </form>
);

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

LoginForm = reduxForm({
  form: 'login',
})(LoginForm);

export default LoginForm;
