import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

class Dropdown extends Component {

  static propTypes = {
    options: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.shape({
        value: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
        ]).isRequired,
        label: PropTypes.string.isRequired,
      }),
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    selected: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }

  constructor(props){
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, true);
  }

  handleToggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  handleClickOutside = (e) => {
    if (!this.node.contains(e.target)) {
      this.setState({
        isOpen: false,
      });
    }
  }

  getLabel = (value) => {
    const { options } = this.props;
    return Array.isArray(options) ? value : options[value];
  }

  render() {
    const { options, selected, onChange } = this.props;
    const values = (Array.isArray(options)) ? options : options.map((option) => option.value);
    const cls = classnames('Dropdown', {
      'is-open': this.state.isOpen,
    });

    return (
      <div
        className={cls}
        ref={(node) => this.node = node} onClick={this.handleToggle}>
        <span className="Dropdown-selected">{selected}</span>
        <ul className="Dropdown-list">
          {values.map((value) => {
            const cls = classnames('Dropdown-option', {
              'is-selected': value === selected,
            });
            return (
              <li
                key={value}
                className={cls}
                onClick={(e) => onChange(value)}>
                {this.getLabel(value)}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default Dropdown;
