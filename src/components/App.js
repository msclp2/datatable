import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../modules/auth/actions';

let App = (props) => (
  <div className="App">
    <button onClick={() => { props.logout() }}>Logout</button>
    {props.children}
  </div>
);

App = connect(
  null,
  {
    logout,
  }
)(App);

export default App;
