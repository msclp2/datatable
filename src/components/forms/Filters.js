import React, { PropTypes } from 'react';
import {
  reduxForm,
  // Form,
  Field,
} from 'redux-form';

import InputField from './components/InputField';
import Button from './components/Button';

let FiltersForm = ({ handleSubmit, onSubmit, submitting }) => (
  <form className="Form Form--filter" onSubmit={handleSubmit(onSubmit)}>
    <div className="Form-field">
      <Field
        component={InputField}
        name="user_filter[name]"
        type="text"
      />
    </div>
    <div className="Form-footer">
      <Button label="Search" type="submit" disabled={submitting} />
    </div>
  </form>
);

FiltersForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

FiltersForm = reduxForm({
  form: 'filters',
  enableReinitialize: true,
})(FiltersForm);

export default FiltersForm;
