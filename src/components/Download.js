import React, { createElement } from 'react';
import Button from './forms/components/Button';
import * as api from '../services/api';

const handleClick = (url) => {
  api.get(url).then((response) => {
    const a = document.createElement('a');
    a.href = response.url;
    a.download = true;
    a.click();
  });
};

const Download = ({ url }) => {
  return (
    <Button className="Download" label="Download CSV" onClick={() => { handleClick(url) }}/>
  );
};



export default Download;
