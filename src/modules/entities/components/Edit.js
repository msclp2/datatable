import React from 'react';
import { connect } from 'react-redux';

let Edit = (props) => (
  <div className="Edit">
    Edit {props.params.id}
  </div>
);

Edit = connect()(Edit);

export default Edit;
