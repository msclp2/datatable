import { queryDecode, queryEncode } from '../services/helpers';

describe('Library', () => {

  it('decode query string correctly', () => {
    expect(queryEncode('b=b&c-c=c-c&a[a]=a[]a')).toEqual({
      b:'b',
      'c-c':'c-c',
      a: {
        a:'a[]a',
      },
    });
    expect(queryEncode('?a=a&b=b&c-c=c-c&')).toEqual({
      a:'a',
      b:'b',
      'c-c':'c-c',
    });
    expect(queryEncode('')).toEqual({});
  });

  it('encode object to query string', () => {
    expect(queryDecode({})).toBe('?');
    expect(queryDecode()).toBe('?');
    expect(queryDecode(null)).toBe('?');
    expect(queryDecode({
      name: 'Joh Doe',
      dateFrom: '01-01-1980',
      '': '',
    })).toBe('?name=Joh%20Doe&dateFrom=01-01-1980');
    expect(queryDecode({
      limit: 10,
      filter: {
        name: 'Jo',
        date: {
          from: '01-01-1980',
          to: '02-02-1980',
        }
      },
    })).toBe('?limit=10&filter%5Bname%5D=Jo&filter%5Bdate%5D%5Bfrom%5D=01-01-1980&filter%5Bdate%5D%5Bto%5D=02-02-1980');
  });
});
