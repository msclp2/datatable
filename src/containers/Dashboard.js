import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  getStaffList,
  getStaffPagination,
  getStaffFilters,
} from '../reducers';
import { staffPageRequest }       from '../modules/entities/actions';
import DataTable from '../components/DataTable';
import Download from '../components/Download';

class Dashboard extends Component {

  componentDidMount() {
    this.fetchData();
  }

  fetchData = (pagination, filters) => {
    const { total, sort, ...p } = this.props.pagination;
    let newSort = sort;
    if (pagination && pagination.column) {
      newSort = (pagination.column === this.props.pagination.column && sort === 'ASC') ? 'DESC' : 'ASC';
    }
    const data = {
      ...p,
      ...pagination,
      sort: newSort,
    };
    const f = filters || this.props.filters;
    const newFilters = {
      user_filter: {
        ...f.user_filter,
        roles: 'ROLE_STAFF',
      },
    };
    this.props.fetchPage(data, newFilters);
  }

  fetchCsvUrl = () => {
    return 'https://api.anpost.ops-dev.pl/users/export';
  }

  render() {
    const {
      users,
      pagination,
      filters,
    } = this.props;
    const tableConfig = {
      columns: [
        { key: 'name', title: 'Name', sortable: true },
        { key: 'email', title: 'Email', sortable: true },
        { key: 'role', title: 'Role' },
        { key: 'created', title: 'Date added', sortable: true },
        { key: 'id', title: 'Details' },
      ],
      pagination,
      filters,
    };

    return (
      <div className="Dashboard">
        <Download url={this.fetchCsvUrl()} />
        <DataTable data={users} config={tableConfig} handler={this.fetchData} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  users: getStaffList(state),
  pagination: getStaffPagination(state),
  filters: getStaffFilters(state),
});

Dashboard = connect(
  mapStateToProps,
  {
    fetchPage: staffPageRequest,
  }
)(Dashboard);

export default Dashboard;
