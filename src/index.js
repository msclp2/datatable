import React                      from 'react';
import { render }                 from 'react-dom';
import { Router, browserHistory } from 'react-router'
import { Provider }               from 'react-redux';
import rootSaga                   from './rootSaga';
import configureStore             from './store/configureStore';
import routes                     from './routes';

const store = configureStore();
store.runSaga(rootSaga);

export { store };

render(
  <Provider store={store}>
    <Router routes={routes} history={browserHistory} />
  </Provider>,
  document.getElementById('root')
);
