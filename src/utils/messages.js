export default {
  'user.password.email.invalid': 'Invalid login credentials',
  'user.disabled': 'Your account is disabled',
}
