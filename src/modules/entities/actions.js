import { createActionTypes, action } from '../factory';

export const ENTITY  = createActionTypes('ENTITY');

export const POSTS = createActionTypes('POSTS');
export const USERS = createActionTypes('USERS');
export const PAGE = createActionTypes('entity/PAGE');


export const entity = {
  request: (endpoint) => action(ENTITY.REQUEST, { endpoint }),
  success: (response) => action(ENTITY.SUCCESS, { response }),
  failure: (error)    => action(ENTITY.FAILURE, { error }),
};

export const page = {
  request: (endpoint, entity, pagination, filters) => action(PAGE.REQUEST, {
    pagination,
    filters,
    meta: {
      endpoint,
      entity,
    }}),
  success: (entity, results, pagination, filters) => action(PAGE.SUCCESS, {
    results,
    pagination,
    filters,
    meta: {
      entity,
    }}),
  failure: (error) => action(PAGE.FAILURE, { error }),
}

const createPageRequestActionCreator = (endpoint, entity) => (pagination, filters) =>
  page.request(endpoint, entity, pagination, filters);

export const staffPageRequest = createPageRequestActionCreator('users', 'staff');
