import { combineReducers } from 'redux';
import { LOGIN, LOGOUT }   from './actions';
import { StorageService }  from '../../services';
import messages            from '../../utils/messages';

const getMessage = (key) => messages[key];

const hasAccessInitialState = Boolean(StorageService.get('token'));

const initialState = {
  token: null,
  user: null,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN.SUCCESS:
      return {
        ...state,
        token: action.response,
      };
    case LOGIN.FAILURE:
      return {
        ...state,
        token: null,
      };
    default:
      return state;
  }
}

// const hasAccess = (state = hasAccessInitialState, action) => {
//   switch (action.type) {
//     case LOGIN.SUCCESS:
//       return true;
//     case LOGIN.FAILURE:
//     case LOGOUT:
//       return false;
//     default:
//       return state;
//   }
// }

// const errorMessage = (state = '', action) => {
//   switch (action.type) {
//     case LOGIN.FAILURE:
//       return action.error;
//     case LOGIN.REQUEST:
//       return null;
//     default:
//       return state;
//   }
// }

export default auth;

// export default combineReducers({
//   hasAccess,
//   errorMessage,
// });

export const isAuthorized = (state) => state.auth.hasAccess;
export const getErrorMessage = (state) => getMessage(state.auth.errorMessage);
