import React, { PropTypes } from 'react';
import {
  reduxForm,
  // Form,
  Field,
} from 'redux-form';

import { required, email } from '../../utils/validation';

import InputField from './components/InputField';
import Button from './components/Button';

let RemindPassword = ({
  handleSubmit,
  onSubmit,
  message,
  submitting,
}) => (
  <form className="Form Form--login" onSubmit={handleSubmit(onSubmit)}>
    <div className="Form-field">
      <Field
        component={InputField}
        validate={[ required, email ]}
        label="Email"
        name="email"
        type="email"
      />
    </div>
    <div className="Form-messages">
      {message && <span>{message}</span>}
    </div>
    <div className="Form-footer">
      <Button label="Login" type="submit" disabled={submitting} />
    </div>
  </form>
);

RemindPassword.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

RemindPassword = reduxForm({
  form: 'remindPassword',
})(RemindPassword);

export default RemindPassword;

