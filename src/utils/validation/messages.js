export default {
  required: 'Required',
  email: 'Invalid email address',
  number: 'Invalid number',
}
