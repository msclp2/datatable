import qs from 'qs';

export const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
};

export const parseData = (response) => response.data;

export const parseJSON = (response) => response.json();

export const queryEncode = (query) => {
  const trimmed = query[0] === '?' && query.slice(1) || query;
  return qs.parse(trimmed);
  if (query === '') {
    return {};
  }
  return trimmed.split('&').reduce((obj, pair) => {
    const splitted = pair.split('=');
    const key = splitted[0];
    const value = splitted[1];
    if (key) obj[key] = value;
    return obj;
  }, {});
};

export const queryDecode = (filterObj) => {
  const query = `?${qs.stringify(filterObj, { skipNulls: true, strictNullHandling: true })}`;
  return (query[query.length-1] === '=') && query.slice(0, -2) || query;
  return Object.keys(filterObj).reduce((query, prop) => {
    let deepQuery = filterObj[prop];
    let filter = '';
    if (typeof filterObj[prop] === 'object') {
      deepQuery = deepDecode(filterObj[prop], prop);
      filter = `${deepQuery}&`;
    } else {
      filter = (filterObj[prop]) ? `${prop}=${filterObj[prop]}&` : '';
    }
    return `${query}${filter}`;
  }, '?').slice(0, -1);
};

function deepDecode(filterObj, parent) {
  if (!filterObj || Object.is(filterObj, {})) {
    return '';
  }
  return Object.keys(filterObj).reduce((query, prop) => {
    if (typeof filterObj[prop] === 'object') {
      return deepDecode(filterObj[prop], prop);
    }
    const filter = (filterObj[prop]) ? `${parent}[${prop}]=${filterObj[prop]}&` : '';
    return `${query}${filter}`;
  }, '').slice(0, -1);
}
