import { fork } from 'redux-saga/effects';
import authSaga from './modules/auth/sagas';
import entitiesSaga from './modules/entities/sagas';

export default function* rootSaga() {
  yield [
    fork(authSaga),
    fork(entitiesSaga),
  ];
}
