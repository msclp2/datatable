import { combineReducers }        from 'redux';
import { routerReducer }          from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import authReducer                from '../modules/auth/reducers';
import entitiesReducer, * as fromEntities from '../modules/entities/reducers';

export default combineReducers({
  auth:     authReducer,
  entities: entitiesReducer,
  form:     formReducer,
  routing:  routerReducer,
});

export const getStaffList = (state) => fromEntities.getEntityList(state.entities, 'staff');
export const getStaffPagination = (state) => fromEntities.getEntityPagination(state.entities, 'staff');
export const getStaffFilters = (state) => fromEntities.getEntityFilters(state.entities, 'staff');
