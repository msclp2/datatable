import {
  call,
  fork,
  put,
  take,
} from 'redux-saga/effects';
import * as api from '../../services/api';
import { page, PAGE } from './actions';


function* fetchEntity(entity, endpoint, pagination, filters) {
  try {
    const { items, total } = yield call(api.get, endpoint, { ...pagination, ...filters });
    yield put(page.success(entity, items, { ...pagination, total: +total }, filters));
  } catch (error) {
    yield put(page.failure(error));
  }
}

function* requestPageWatcher() {
  while (true) {
    const { pagination, filters, meta: { endpoint, entity } } = yield take(PAGE.REQUEST);
    yield call(fetchEntity, entity, endpoint, pagination, filters);
  }
}

export default function* rootSaga() {
  yield [
    fork(requestPageWatcher),
  ];
}
