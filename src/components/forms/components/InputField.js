import React, { PropTypes } from 'react';
import classnames from 'classnames';

const InputField = ({
  input,
  label,
  meta: {
    error,
    invalid,
    touched,
    warning,
  },
  ...props
}) => {
  const cls = classnames('InputField', {
    'is-invalid': invalid,
    // 'is-valid': meta.valid,
  });
  const labelCls = classnames('InputField-label');

  return (
    <div className={cls}>
      {label && <label className={labelCls}>{label}</label>}
      <input {...input} {...props} />
      <div className="InputField-validation">
        {touched && 
          ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))
        }
      </div>
    </div>
  );
};

InputField.propTypes = {
  label: PropTypes.string,
};

export default InputField;
