import { browserHistory } from 'react-router';
import { delay }       from 'redux-saga';
import {
  call,
  fork,
  put,
  race,
  take,
}                      from 'redux-saga/effects';
import { AuthService } from '../../services';
import {
  login,
  logout,
  remindPassword,
  LOGIN,
  LOGOUT,
  REMIND_PASSWORD,
}                      from './actions';

function* authorize(credentialsOrToken) {
  try {
    const { token } = yield race({
      token: call(AuthService.authorize, credentialsOrToken),
      logout: take(LOGOUT),
    });
    if (token) {
      yield call(AuthService.setToken, token);
      yield put(login.success(token));
      yield call(forwardTo, '/');
      return token;
    }
  } catch (error) {
    yield put(login.failure(error.response.data.message));
  }
  return null;
}

function* remind(credentials) {
  const { message } = yield call(AuthService.remindPassword, credentials);
  // try {
  // } catch (error) {
  // }
}

function* signout(message) {
  yield call(AuthService.removeToken);
  yield put(logout(message));
  yield call(forwardTo, '/auth/login');
}

function* authorizationWatcher() {
  let token = yield call(AuthService.getToken);
  if (token) {
    token.expires_in = 0;
  }

  while (true) {
    if (!token) {
      const { credentials } = yield take(LOGIN.REQUEST);
      token = yield call(authorize, credentials);
      if (!token) continue;
    }

    while (token) {
      const { expired } = yield race({
        expired: call(delay, token.expires_in * 1000),
        logout: take(LOGOUT),
      });
      if (expired) {
        token = yield call(authorize, token);
        if (!token) {
          yield call(signout);
        }
      } else {
        token = null;
        yield call(signout);
      }
    }
  }
}

function* resetPasswordWatcher() {
  while (true) {
    const { credentials } = yield take(REMIND_PASSWORD.REQUEST);
  }
}

export default function* rootSaga() {
  yield [
    fork(authorizationWatcher),
    fork(resetPasswordWatcher),
  ]
}

function forwardTo (location) {
  browserHistory.push(location);
}
