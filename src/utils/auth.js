import { StorageService } from '../services';
import { isAuthorized }   from '../modules/auth/reducers';
import { store } from '../';

export const checkAuthorization = ({ params, dispatch }, replace) => {
  const token = StorageService.get('token');

  if (token) {
    const { created } = token;
    const ttl = 1209600;
    const expiry = created + ttl;

    if (created > expiry) {
      return false;
    }

    return true;
  }

  return false;
};

export const checkPanelAuthorization = ({ params }, replace) => { // (nextState, replace, next) => {
  // const { hasAccess } = getState().auth;
  const token = StorageService.get('token');

  if (token) {
    return true;
  }

  return false;

  // if (hasAccess) {
  //   return next():
  // }

  replace('/auth/login');
};
export const isAuthenticated = ({ params }, replace) => {
  const isLoggedIn = store.getState().auth.token;
  if (!isLoggedIn) {
    replace('/auth/login');
  }
}
