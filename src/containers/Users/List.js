import React, { Component } from 'react';
import { connect } from 'react-redux';

class UsersList extends Component {

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
  }

  render() {
    const { users } = this.props;

    return (
      <div className="UsersList">
        Users list
        <ul>
          {users && users.map((user) => <li key={user.id}>User</li>)}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  users: [],
})

UsersList = connect(mapStateToProps)(UsersList);

export default UsersList;
