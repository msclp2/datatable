const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';

export const createActionTypes = (base) =>
  [REQUEST, SUCCESS, FAILURE].reduce((accu, type) => {
    accu[type] = `${base}_${type}`;
    return accu;
  }, {});

export const action = (type, payload = {}) => ({ type, ...payload });

